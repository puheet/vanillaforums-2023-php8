# vanillaforums-2023-php8

Vanilla Forums 2023.001 pseudo-release with PHP8 support

Based on the [vanilla-2023.001](https://open.vanillaforums.com/discussion/39237/vanilla-2023-001-open-source-release-php-8-works-built-from-source) release, with a bunch of fixes added on top.

Credit to user [vanilla_2022](https://open.vanillaforums.com/profile/vanilla_2022) for the above.

The original version with no additional fixes on top was downloaded from the link below:

[Download vanilla-2023-001](https://us.v-cdn.net/5018160/uploads/PVCJPS93HS8R/vanilla-2023-001.zip)

(The download link was last tested on 2023-11-22 and it does still work, but you never know.)

[Archived vanillaforums-2023-php8 on GitHub](https://github.com/banaanihillo/vanillaforums-2023-php8)
