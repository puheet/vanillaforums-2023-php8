<?php if (!defined('APPLICATION')) exit();

// Cache
$Configuration['Cache']['Enabled'] = true;
$Configuration['Cache']['Method'] = 'memcached';
$Configuration['Cache']['Memcached']['Store'] = array (
  0 => 'localhost:11211',
);

// Context
$Configuration['Context']['Secret'] = 'tBPsUTiQdTgP6jcivDBX6IX6bMtQFeiu';

// Database
$Configuration['Database']['Name'] = 'vanilla2023';
$Configuration['Database']['Host'] = 'localhost';
$Configuration['Database']['User'] = 'vanillaUser';
$Configuration['Database']['Password'] = 'pantry unluckily retying drapery gumminess daisy survivor';
$Configuration['Debug'] = true;

// EnabledApplications
$Configuration['EnabledApplications']['Conversations'] = 'conversations';
$Configuration['EnabledApplications']['Vanilla'] = 'vanilla';

// EnabledPlugins
$Configuration['EnabledPlugins']['stubcontent'] = false;
$Configuration['EnabledPlugins']['swagger-ui'] = true;
$Configuration['EnabledPlugins']['Quotes'] = false;
$Configuration['EnabledPlugins']['rich-editor'] = true;
$Configuration['EnabledPlugins']['VanillaStats'] = true;
$Configuration['EnabledPlugins']['vanillicon'] = true;
$Configuration['EnabledPlugins']['yaga'] = true;
$Configuration['EnabledPlugins']['Multilingual'] = true;
$Configuration['EnabledPlugins']['DiscussionPolls'] = true;
$Configuration['EnabledPlugins']['PuheetPlatformIntegration'] = true;

// Garden
$Configuration['Garden']['Title'] = 'Vanilla';
$Configuration['Garden']['Cookie']['Salt'] = '4wClQZQUGjI4XKDv8f7rRK4BSjimK5B7';
$Configuration['Garden']['Cookie']['Domain'] = '';
$Configuration['Garden']['Registration']['ConfirmEmail'] = false;
$Configuration['Garden']['Email']['SupportName'] = 'Vanilla';
$Configuration['Garden']['Email']['Format'] = 'text';
$Configuration['Garden']['UpdateToken'] = 'aa657b4b4608f4cf2f954a3e26dbdb54002bd6e8';
$Configuration['Garden']['Scheduler']['Token'] = 'SchedulerToken6412c9afd78bb3.02633800';
$Configuration['Garden']['SystemUserID'] = '1';
$Configuration['Garden']['InputFormatter'] = 'rich';
$Configuration['Garden']['Themes']['Visible'] = 'theme-foundation';
$Configuration['Garden']['Version'] = 'Undefined';
$Configuration['Garden']['CanProcessImages'] = true;
$Configuration['Garden']['MobileInputFormatter'] = 'rich';
$Configuration['Garden']['Installed'] = true;
$Configuration['Garden']['Theme'] = 'puheet';
$Configuration['Garden']['Profile']['EditUsernames'] = true;
$Configuration['Garden']['Profile']['EditEmails'] = false;
$Configuration['Garden']['User']['ValidationLength'] = '{3,50}';

// Plugins
$Configuration['Plugins']['Vanillicon']['Type'] = 'v2';
$Configuration['Plugins']['PuheetPlatformIntegration']['ForumSecretKey'] = 'erupt upfront baggage unsaved slinging flask plexiglas';
$Configuration['Plugins']['PuheetPlatformIntegration']['PuheetPlatformBackendUrl'] = 'https://community.omakone.local/api';
$Configuration['Plugins']['PuheetPlatformIntegration']['PuheetPlatformFrontendUrl'] = 'https://community.omakone.local/auth';
$Configuration['Plugins']['PuheetPlatformIntegration']['PuheetPlatformIdentityManagementSystem'] = 'empty';
$Configuration['Plugins']['PuheetPlatformIntegration']['PuheetPlatformIdentityUsername'] = 'empty';
$Configuration['Plugins']['PuheetPlatformIntegration']['PuheetPlatformIdentityPassword'] = 'empty';
$Configuration['Plugins']['PuheetPlatformIntegration']['PuheetPlatformNavbarUrl'] = 'https://community.omakone.local/nav';
$Configuration['Plugins']['PuheetPlatformIntegration']['CommentsReloading'] = '0';
$Configuration['Plugins']['PuheetPlatformIntegration']['ExternalIAM'] = '0';
$Configuration['Plugins']['PuheetPlatformIntegration']['ExternalSignInUrl'] = 'https://customer.fi/login?target=https://forum-customer.puheet.info';
$Configuration['Plugins']['PuheetPlatformIntegration']['ExternalSignOutUrl'] = 'https://customer.fi/logout';

// RichEditor
$Configuration['RichEditor']['Quote']['Enable'] = true;

// Routes
$Configuration['Routes']['YXBwbGUtdG91Y2gtaWNvbi5wbmc='] = array (
  0 => 'utility/showtouchicon',
  1 => 'Internal',
);
$Configuration['Routes']['cm9ib3RzLnR4dA=='] = array (
  0 => '/robots',
  1 => 'Internal',
);
$Configuration['Routes']['dXRpbGl0eS9yb2JvdHM='] = array (
  0 => '/robots',
  1 => 'Internal',
);
$Configuration['Routes']['Y29udGFpbmVyLmh0bWw='] = array (
  0 => 'staticcontent/container',
  1 => 'Internal',
);
$Configuration['Routes']['DefaultController'] = 'discussions';

// Vanilla
$Configuration['Vanilla']['RoleToken']['Secret'] = '69866e7e6ce80a01153b46b588484c8194591b44';
$Configuration['Vanilla']['Password']['SpamCount'] = 2;
$Configuration['Vanilla']['Password']['SpamTime'] = 1;
$Configuration['Vanilla']['Password']['SpamLock'] = 120;