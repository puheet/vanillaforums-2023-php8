let showPuheetLoadCommentsBox = false;
let newestPuheetUiCommentTime = null;
let puheetLoadCommentsInterval = null;
let buttonText = 'Refresh page';
let messageText = 'New comments available! Refresh the page to load them.';
let isFirstOnLoadEvent = true;
let topicHasNoComments = false;

function setDefaultPuheetPluginStyle() {
    // This is just the default style and is not supposed to be edited. Create your own style inside a theme.
    const puheetSyle = document.createElement('style');
    puheetSyle.innerHTML = '.puheet-forum-new-comments-container { visibility: hidden; } .puheet-forum-new-comments-container-visible { visibility: visible; }';
    document.querySelector('head').append(puheetSyle);
}

function getQueryString() {
    let queryString = '';
    const firstComment = document.querySelector('ul.MessageList.DataList.Comments > li');

    if (firstComment) {
        const commentId = firstComment.id.split('_')[1];
        queryString = '?commentId=' + commentId;
    } else {
        const getDiscussionIdRegExp = /(?<=\/discussion\/)[0-9]+(?=\/)/gm;
        const discussionId = window.location.pathname.match(getDiscussionIdRegExp).toString();
        queryString = '?discussionId=' + discussionId;
    }

    const cacheBuster = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 8);
    return queryString + '&bust=' + cacheBuster;
}

function getNewestCommentTime() {
    const url = window.location.origin + '/puheet/getTimeOfNewestComment' + getQueryString();

    $.get(url, function(response) {
        showPuheetLoadCommentsBox = true;
        const newestServerCommentTime = response.latestComment.timestamp.date;
        const commentIsNotOwn = !response.latestComment.isOwn;

        if (topicHasNoComments && newestServerCommentTime && commentIsNotOwn) {
            showCommentsNotification();
        } else if (!newestServerCommentTime) {
            topicHasNoComments = true;
        } else if (!newestPuheetUiCommentTime) {
            newestPuheetUiCommentTime = newestServerCommentTime;
        } else if (newestPuheetUiCommentTime < newestServerCommentTime && commentIsNotOwn) {
            showCommentsNotification();
        }
    });
}

function showCommentsNotification() {
    document.querySelector('.puheet-forum-new-comments-container').classList.add('puheet-forum-new-comments-container-visible');
    clearInterval(puheetLoadCommentsInterval);
}

function createLoadCommentsContainer() {
    const puheetLoadCommentsContainer = document.createElement('div');
    puheetLoadCommentsContainer.className = 'puheet-forum-load-comments-row';

    return puheetLoadCommentsContainer;
}

function createLoadCommentsButton() {
    const puheetLoadCommentsButton = document.createElement('button');
    puheetLoadCommentsButton.className = 'Button puheet-forum-load-comments-button';
    puheetLoadCommentsButton.innerText = buttonText;
    puheetLoadCommentsButton.onclick = function(event) {
        event.preventDefault();

        if (location.href.indexOf('#latest') < 0) {
            location.href = location.href + '#latest';
        }

        location.reload();
    };

    return puheetLoadCommentsButton;
}

function createNewCommentsAvailableMessage() {
    const puheetNewCommentsAvailableMessage = document.createElement('span');
    puheetNewCommentsAvailableMessage.className = 'puheet-forum-new-comments-container';
    puheetNewCommentsAvailableMessage.innerText = messageText;

    return puheetNewCommentsAvailableMessage;
}

function setupPuheetLoadCommentsPlugin() {
    if (document.querySelector('.puheet-forum-load-comments-row')) {
        return; // already set up!
    }

    // load default styles for this plugin
    setDefaultPuheetPluginStyle();

    // set language
    setLanguage();

    // create needed plugin elements
    const puheetLoadCommentsContainer = createLoadCommentsContainer();
    const puheetLoadCommentsButton = createLoadCommentsButton();
    const puheetNewCommentsAvailableMessage = createNewCommentsAvailableMessage();

    puheetLoadCommentsContainer.appendChild(puheetNewCommentsAvailableMessage);
    puheetLoadCommentsContainer.appendChild(puheetLoadCommentsButton);

    // attach plugin elements to the DOM
    const puheetContainer = document.querySelector('.CommentForm');
    puheetContainer.prepend(puheetLoadCommentsContainer);

    // get newest comment timestamp after 10 seconds
    puheetLoadCommentsInterval = setInterval(getNewestCommentTime, 10000);
}

function scrollToCommentsEnd() {
    const latest = document.getElementById('latest');

    if (location.href.indexOf('#latest') > 0 && latest) {
        latest.scrollIntoView();
    }
}

function setLanguage() {
    const sample = document.querySelector('a[href="/discussions"]').text.trim().toUpperCase();

    buttonText = 'Refresh page';
    messageText = 'New comments available! Refresh the page to see them.';

    if (sample === 'KESKUSTELUT') {
        buttonText = 'Päivitä sivu';
        messageText = 'Uusia kommentteja saatavilla! Päivitä sivu nähdäksesi ne.';
    } else if (sample === 'DISKUSSIONER') {
        buttonText = 'Uppdatera';
        messageText = 'Nya kommentarer tillgängliga! Uppdatera sidan för att se dem.';
    } else if (sample === 'DISKUSSIONEN') {
        buttonText = 'Seite neuladen';
        messageText = 'Neue Kommentare verfügbar! Laden Sie die Seite neu, um sie anzuzeigen.';
    }
}

$(document).on('contentLoad', function () {
    getNewestCommentTime();
    setupPuheetLoadCommentsPlugin();

    if (isFirstOnLoadEvent) {
        isFirstOnLoadEvent = false;
        scrollToCommentsEnd();
    }
});

// To configure how often a draft is saved edit applications/vanilla/js/autosave.js
