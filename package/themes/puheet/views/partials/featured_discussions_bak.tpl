<div class="featured-discussions-container">
<h1 class="featured-discussions-title">{t c="Featured discussions"}</h1>
<div class="featured-discussions-items">

{foreach $FeaturedDiscussions as $discussion}
    <div class="featured-discussions-item">
        <h4 class="item-title">{$discussion['Name']}</h4>
        <div class="item-tag">{$discussion['Tag']}</div>
    </div>
{/foreach}

<div class="featured-discussions-item first">
    <h4 class="item-title">Here's what we found from the customer survey</h4>
    <div class="item-tag">Survey</div>
</div>
<div class="featured-discussions-item second">
    <h4 class="item-title">Be among the first to get Apple Pay</h4>
    <div class="item-tag">Payments</div>
</div>
<div class="featured-discussions-item third last">
    <h4 class="item-title">Be among the first to get Apple Pay</h4>
    <div class="item-tag">Survey</div>
</div>
</div>
</div>
<div style="margin-top: 40px"> </div>
<style>
  .featured-discussions-container {
    margin: 40px 0;
    width: 100%;
  }
  .featured-discussions-title {
    margin: 10px 0 20px 0;
  }
  .featured-discussions-items {
    display: flex;
    flex-wrap: nowrap;
    justify-content: space-between;
  }
  .featured-discussions-item {
    flex-grow: 1;
    flex-basis: 33.3%;
    max-width: 31.5%;
    min-height: 180px;
    padding: 1.5% 2.5%;
    position: relative;
    display: block;
    transition: transform .2s;
    cursor: pointer;
  }

  @media only screen and (max-width: 768px) {
    .featured-discussions-item {
      max-width: 49%;
    }
    .featured-discussions-item.last {
      display: none;
    }
  }

  .featured-discussions-item:hover:before {
     opacity: 0.8;
     border-color: #c77827;
  }

  .featured-discussions-item:hover:before,
  .featured-discussions-item:hover .item-title,
  .featured-discussions-item:hover .item-tag {
     transition: transform .3s;
     transform: scale(1.04);
  }
  .featured-discussions-item .item-tag {
    text-shadow: 0 0 10px rgba(0,0,0,0.55);
    box-shadow: 0px 0px 4px rgba(0,0,0,0.4);
  }
  .featured-discussions-item.first:before {
    background-image: url('uploads/editor/ez/4snkgisk36dy.jpg');
  }
  .featured-discussions-item.second:before {
    background-image: url('uploads/editor/uc/y6s7x40yayo5.jpg');
  }
  .featured-discussions-item.third:before {
    background-image: url('uploads/editor/u1/nlal8vl5jzy4.jpg');
  }
  .featured-discussions-item:before {
    content: " ";
    padding-top: 56.25%;
    background-image: url('uploads/editor/vx/7lu4t7zl71h0.jpg');
    background-repeat: no-repeat;
    background-position: 50% 0;
    -ms-background-size: cover;
    -o-background-size: cover;
    -moz-background-size: cover;
    -webkit-background-size: cover;
    background-size: cover;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    position: absolute;
    z-index: 0;
    border: 1px solid #333;
  }

  .featured-discussions-item .item-title {
    z-index: 2;
    position: relative;
    top: 10px;
    color: black;
    font-size: 24px;
    text-shadow: 0px 0px 10px rgba(255,255,255,0.55);
  }

  .item-tag {
    text-transform: uppercase;
    color: white;
    font-weight: 600;
    padding: 4px 16px;
    font-size: 15px;
    border-radius: 16px;
    border: 2px solid white;
    position: relative;
    top: 30px;
    display: inline-block;
  }

</style>
