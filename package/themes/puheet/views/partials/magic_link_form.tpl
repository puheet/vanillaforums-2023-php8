<div>
    {t c="Order a one-time sign-in/registration link."}
    <form id="magicLinkForm">
        <input type="text" id="Form_Email" name="magicLinkEmail" placeholder="Email" value=""><br>
        <div class="BoxButtons BoxMagicLink">
            <button type="button" class="magicLinkButton Button Primary Action MagicLinkButton BigButton">
                {t c="Order Link"}
            </button>
        </div>
    </form>
    <div class="magicLinkResponse">
        <p class="magicLinkResponseText">
            &nbsp;
        </p>
    </div>
    <div class="magicLinkSpinner">
        <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
    </div>
</div>
<script>
    // TODO translaatiot
    const magicLinkFormButton = document.querySelector('.magicLinkButton');
    const magicLinkResponseElement = document.querySelector('.magicLinkResponse');
    const responseElement = document.querySelector('.magicLinkResponse');
    const responseTextElement = document.querySelector('.magicLinkResponseText');
    const magicLinkSpinner = document.querySelector('.magicLinkSpinner');
    const magicLinkTextField = document.querySelector('input[name="magicLinkEmail"]');


    magicLinkFormButton.onclick = function(event) {
        event.preventDefault();
        sendMagicLink();
    };

    magicLinkTextField.addEventListener("keydown", function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            sendMagicLink();
        }
    });

    function sendMagicLink() {
        const magicLinkEmail = magicLinkTextField.value;

        if (!magicLinkEmail.length || !validateEmail(magicLinkEmail)) return;

        waitForMagicLinkResponse();
        const magicLinkUrl = '{$PlatformUrl}' + '/send_magic_link';
        const magicLinkTarget = window.location.href;
        const magicLinkJson = JSON.stringify({
            target: magicLinkTarget,
            email: magicLinkEmail,
        });
        const xhr = new XMLHttpRequest();
        xhr.open('POST', magicLinkUrl, true);
        xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');

        xhr.onreadystatechange = handleMagicLinkResponse;
        xhr.send(magicLinkJson);
    }

    function handleMagicLinkResponse() {
        hideMagicLinkSpinner();
        magicLinkResponseElement.classList.add('magicLink--visible');

        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            responseTextElement.innerHTML = '{t c="All done, check your mail!"}';
        } else if (this.status >= 400) {
            responseTextElement.innerHTML = '{t c="Sending a link failed."}';
        }
    }

    function validateEmail(email) {
        const re = /^\S+@\S+$/; // very basic validation
        return re.test(email);
    }

    function showMagicLinkSpinner() {
        magicLinkSpinner.classList.add('magicLink--visible');
        magicLinkResponseElement.classList.add('magicLink--hidden');
    }

    function hideMagicLinkSpinner() {
        magicLinkSpinner.classList.remove('magicLink--visible');
        magicLinkResponseElement.classList.remove('magicLink--hidden');
    }

    function waitForMagicLinkResponse() {
        magicLinkResponseElement.classList.add('magicLink--hidden');
        responseTextElement.innerHTML = '&nbsp;';
        showMagicLinkSpinner();
    }

</script>
<style>
    div.BoxButtons.BoxMagicLink {
        margin: 0;
    }

    .magicLinkResponse {
        height: 45px;
        margin-bottom: 18px;
    }

    .magicLinkResponseText {
        margin: 0;
    }

    .magicLinkSpinner {
        height: 45px;
        margin-bottom: 18px;
        display: none;
    }

    .magicLink--visible {
        display: block;
    }

    .magicLink--hidden {
        display: none;
    }

    input#Form_Email {
        margin-top: 12px;
        margin-bottom: 18px;
    }

    .lds-ring {
        display: inline-block;
        position: relative;
        width: 40px;
        height: 40px;
    }
    .lds-ring div {
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        display: block;
        position: absolute;
        width: 32px;
        height: 32px;
        margin: 4px;
        border: 4px solid rgb(255, 79, 79);
        border-radius: 50%;
        -webkit-animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
        animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
        border-color: rgb(255, 79, 79) transparent transparent transparent;
    }
    .lds-ring div:nth-child(1) {
        -webkit-animation-delay: -0.45s;
        animation-delay: -0.45s;
    }
    .lds-ring div:nth-child(2) {
        -webkit-animation-delay: -0.3s;
        animation-delay: -0.3s;
    }
    .lds-ring div:nth-child(3) {
        -webkit-animation-delay: -0.15s;
        animation-delay: -0.15s;
    }
    @-webkit-keyframes lds-ring {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
    @keyframes lds-ring {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
</style>