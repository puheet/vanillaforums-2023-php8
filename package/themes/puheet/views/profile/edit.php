<!--followed this Vanilla's discussion https://open.vanillaforums.com/discussion/18147/how-to-disable-users-to-edit-their-emails
This file is copied from Vanilla's core  /application/dashboard/views/profile/edit.php
Vanilla tries first custom plugins so coping edit.php file here makes it rendered here with customized form fields
Email input and show email to other users checkbox is removed because editing them in Vanilla doesn't change anything -->
<?php if (!defined('APPLICATION')) exit(); ?>
<div class="FormTitleWrapper">
    <h1 class="H"><?php echo $this->data('Title'); ?></h1>
    <?php
    echo $this->Form->open();
    echo $this->Form->errors();
    ?>
    <ul>
        <li class="User-Name">
            <?php
            echo $this->Form->label('Username', 'Name');
            $Attributes = [];

            if (!$this->data('_CanEditUsername')) {
                $Attributes['disabled'] = 'disabled';
            }
            echo $this->Form->textBox('Name', $Attributes);
            ?>
        </li>

        <?php if ($this->data('_CanConfirmEmail')): ?>
            <li class="User-ConfirmEmail">
                <?php
                echo $this->Form->checkBox('ConfirmEmail', t("Confirmed email address"), ['value' => '1']);
                ?>
            </li>
        <?php endif ?>

        <?php if (c('Garden.Profile.Titles', false)): ?>
            <li class="User-Title">
                <?php
                echo $this->Form->label('Title', 'Title');
                echo $this->Form->textBox('Title');
                ?>
            </li>
        <?php endif; ?>

        <?php if (c('Garden.Profile.Locations', false)): ?>
            <li class="User-Location">
                <?php
                echo $this->Form->label('Location', 'Location');
                echo $this->Form->textBox('Location');
                ?>
            </li>
        <?php endif; ?>

        <?php
        $this->fireEvent('EditMyAccountAfter');
        ?>
    </ul>
    <?php echo $this->Form->close('Save', '', ['class' => 'Button Primary']); ?>
</div>
