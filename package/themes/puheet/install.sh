#!/bin/bash
set -e

sudo rm -rf /var/www/forum/themes/puheet
sudo mkdir /var/www/forum/themes/puheet
# Do not copy anything that starts with dot, such as .git/
sudo cp -r * /var/www/forum/themes/puheet/

sudo chown -R www-data:www-data /var/www/forum/themes/puheet
sudo rm -rf /var/www/forum/cache/*
sudo service nginx reload
