<?php return \Vanilla\Web\Asset\WebpackAssetDefinitionCollection::__set_state(array(
   'section' => 'admin-new',
   'jsAssetsByAddonKey' => 
  array (
    'dashboard' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/admin-new/async/addons/dashboard-common.61e936e11f20aed53b8e.min.js',
         'assetType' => 'js',
         'section' => 'admin-new',
         'addonKey' => 'dashboard',
      )),
      1 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/admin-new/async/addons/dashboard.b84537f1432285ffd0c7.min.js',
         'assetType' => 'js',
         'section' => 'admin-new',
         'addonKey' => 'dashboard',
      )),
    ),
    'vanilla' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/admin-new/async/addons/vanilla.99d6c950178df8d8e8df.min.js',
         'assetType' => 'js',
         'section' => 'admin-new',
         'addonKey' => 'vanilla',
      )),
    ),
    'reactions' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/admin-new/async/addons/reactions.c77019b82abab53d41d0.min.js',
         'assetType' => 'js',
         'section' => 'admin-new',
         'addonKey' => 'reactions',
      )),
    ),
  ),
   'cssAssetsByAddonKey' => 
  array (
    'library' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/admin-new/async/73c710f04d6191506aad.min.css',
         'assetType' => 'css',
         'section' => 'admin-new',
         'addonKey' => 'library',
      )),
    ),
  ),
   'jsAssetsGlobal' => 
  array (
    0 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/packages.99a05bb169c6e958a791.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    1 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/vendors.ecf62a238e241399cd71.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    2 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-e2ae3a23.04d78058152c03ed2480.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    3 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/1748.999fef27bd8a8cb3e580.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    4 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-cc102d41.80f8a3ee943b6a4c42b9.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    5 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-de8b5b60.144222cfbb6893c046e6.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    6 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-155ed1d1.44ebc6a998e5d5a74464.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    7 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-b3ff4ff5.2f10c1079d1d0d334590.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    8 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-0074f8f6.6487b489e16d1aead5f6.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    9 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-2c9e96e5.942de90ee483940427c0.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    10 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-154f14ca.d7f3d10cac5a06721a73.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    11 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-222bc760.bbd1936d503573d45f9d.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    12 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-a48e544a.2c25fe2ec493d6f3dcd3.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    13 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-d4f2673a.becc6587c4d4b7cc53ad.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    14 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-b5206d0f.4e69c5c5abc3afa88437.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    15 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-2f1652fc.24bde5e6c9bf3d496a0d.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    16 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-64840acf.9547df390ef1030b9053.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    17 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-9aa6fe4f.7425cc9249e53640ef9f.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    18 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-c19d9c43.560aee4a3f12e3ba4c82.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    19 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-c7c1a4dd.6fd0f87e03a335fc72ec.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    20 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-8351907e.fdfd3b750fb9523e54ae.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    21 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-30ddcb81.709901734f12952b513f.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    22 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-3b962b3c.70501e2c5042a9ba37b3.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    23 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-91c7739a.e779b8bdeeadec8fdc9d.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    24 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-1b5f44f5.0649833aeafb6e0902ef.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    25 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-cb657800.c54e361c592bd0c1aaaf.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    26 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-2da6cbf1.1434b28e0c900f1cc62d.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    27 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-3f9d9fa6.84e963a101a463c721ca.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    28 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-78a760e5.b712b750911bfe426b91.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    29 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-1f0b08b7.384d3c49cec7edf70d29.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    30 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-fae66388.c171d269ec9deb2888b2.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    31 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-35a1f0cc.674ed8028e0019773138.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    32 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-d51a67ca.59a2e1a5bb0e9ae9659a.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    33 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-b2a39f78.11d878c3dc504ace8182.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
    34 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin-new/library-70b9322c.61f849e50dfd9224c756.min.js',
       'assetType' => 'js',
       'section' => 'admin-new',
       'addonKey' => NULL,
    )),
  ),
   'cssAssetsGlobal' => NULL,
   'runtimeJsAsset' => 
  \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
     'assetPath' => '/dist/v1/admin-new/runtime.42f75244fa85125ec092.min.js',
     'assetType' => 'js',
     'section' => 'admin-new',
     'addonKey' => NULL,
  )),
   'bootstrapJsAsset' => 
  \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
     'assetPath' => '/dist/v1/admin-new/bootstrap.8ae1010dd6ff05e5ea5c.min.js',
     'assetType' => 'js',
     'section' => 'admin-new',
     'addonKey' => NULL,
  )),
   'allAssetUrls' => 
  array (
    '/dist/v1/admin-new/async/73c710f04d6191506aad.min.css' => true,
    '/dist/v1/admin-new/async/addons/dashboard-common.61e936e11f20aed53b8e.min.js' => true,
    '/dist/v1/admin-new/async/addons/dashboard.b84537f1432285ffd0c7.min.js' => true,
    '/dist/v1/admin-new/async/addons/vanilla.99d6c950178df8d8e8df.min.js' => true,
    '/dist/v1/admin-new/async/addons/reactions.c77019b82abab53d41d0.min.js' => true,
    '/dist/v1/admin-new/packages.99a05bb169c6e958a791.min.js' => true,
    '/dist/v1/admin-new/vendors.ecf62a238e241399cd71.min.js' => true,
    '/dist/v1/admin-new/library-e2ae3a23.04d78058152c03ed2480.min.js' => true,
    '/dist/v1/admin-new/1748.999fef27bd8a8cb3e580.min.js' => true,
    '/dist/v1/admin-new/library-cc102d41.80f8a3ee943b6a4c42b9.min.js' => true,
    '/dist/v1/admin-new/library-de8b5b60.144222cfbb6893c046e6.min.js' => true,
    '/dist/v1/admin-new/library-155ed1d1.44ebc6a998e5d5a74464.min.js' => true,
    '/dist/v1/admin-new/library-b3ff4ff5.2f10c1079d1d0d334590.min.js' => true,
    '/dist/v1/admin-new/library-0074f8f6.6487b489e16d1aead5f6.min.js' => true,
    '/dist/v1/admin-new/library-2c9e96e5.942de90ee483940427c0.min.js' => true,
    '/dist/v1/admin-new/library-154f14ca.d7f3d10cac5a06721a73.min.js' => true,
    '/dist/v1/admin-new/library-222bc760.bbd1936d503573d45f9d.min.js' => true,
    '/dist/v1/admin-new/library-a48e544a.2c25fe2ec493d6f3dcd3.min.js' => true,
    '/dist/v1/admin-new/library-d4f2673a.becc6587c4d4b7cc53ad.min.js' => true,
    '/dist/v1/admin-new/library-b5206d0f.4e69c5c5abc3afa88437.min.js' => true,
    '/dist/v1/admin-new/library-2f1652fc.24bde5e6c9bf3d496a0d.min.js' => true,
    '/dist/v1/admin-new/library-64840acf.9547df390ef1030b9053.min.js' => true,
    '/dist/v1/admin-new/library-9aa6fe4f.7425cc9249e53640ef9f.min.js' => true,
    '/dist/v1/admin-new/library-c19d9c43.560aee4a3f12e3ba4c82.min.js' => true,
    '/dist/v1/admin-new/library-c7c1a4dd.6fd0f87e03a335fc72ec.min.js' => true,
    '/dist/v1/admin-new/library-8351907e.fdfd3b750fb9523e54ae.min.js' => true,
    '/dist/v1/admin-new/library-30ddcb81.709901734f12952b513f.min.js' => true,
    '/dist/v1/admin-new/library-3b962b3c.70501e2c5042a9ba37b3.min.js' => true,
    '/dist/v1/admin-new/library-91c7739a.e779b8bdeeadec8fdc9d.min.js' => true,
    '/dist/v1/admin-new/library-1b5f44f5.0649833aeafb6e0902ef.min.js' => true,
    '/dist/v1/admin-new/library-cb657800.c54e361c592bd0c1aaaf.min.js' => true,
    '/dist/v1/admin-new/library-2da6cbf1.1434b28e0c900f1cc62d.min.js' => true,
    '/dist/v1/admin-new/library-3f9d9fa6.84e963a101a463c721ca.min.js' => true,
    '/dist/v1/admin-new/library-78a760e5.b712b750911bfe426b91.min.js' => true,
    '/dist/v1/admin-new/library-1f0b08b7.384d3c49cec7edf70d29.min.js' => true,
    '/dist/v1/admin-new/library-fae66388.c171d269ec9deb2888b2.min.js' => true,
    '/dist/v1/admin-new/library-35a1f0cc.674ed8028e0019773138.min.js' => true,
    '/dist/v1/admin-new/library-d51a67ca.59a2e1a5bb0e9ae9659a.min.js' => true,
    '/dist/v1/admin-new/library-b2a39f78.11d878c3dc504ace8182.min.js' => true,
    '/dist/v1/admin-new/library-70b9322c.61f849e50dfd9224c756.min.js' => true,
    '/dist/v1/admin-new/56502cc5825eb8e7f3e79545ab11a008.svg' => true,
    '/dist/v1/admin-new/6a230fef45e77d218793.png' => true,
    '/dist/v1/admin-new/24dfe39c31bfab4dc619.png' => true,
    '/dist/v1/admin-new/619bc298cfc2a42ba6427ad2a10a8858.svg' => true,
  ),
));
